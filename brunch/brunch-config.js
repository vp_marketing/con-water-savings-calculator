// See http://brunch.io for documentation.

module.exports = {
    paths: {
      public: '../public'
    },
    modules: {
      autoRequire: {
        // outputFileName : [ entryModule ]
        'app.js': ['initialize']
      }
    },
    plugins: {
      sass: {
        options: {
          includePaths: [
            'node_modules/bootstrap/scss'
          ]
        }
      }
    },
    files: {
      javascripts: { joinTo: 'app.js' },
      stylesheets: { joinTo: 'app.css' }
    },
  }
  