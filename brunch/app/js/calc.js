'use strict';
const $ = require('jquery');

let calc_values = {
    number_beds: 0,
    pads_per_bed_per_day: 0,
    days_mopped_per_week: 0,
    weeks_mopped_per_year: 0,
    cost_electrical: 0.1042,
    cost_water: 0.00866,
    energy_cost_savings: 0,
    water_cost_savings: 0,
    water_consumption_savings: 0,
    energy_consumption_savings: 0,
    economic_impact_three: 0,
    economic_impact_five: 0,
    economic_impact_ten: 0,
    environmental_impact_three: 0,
    environmental_impact_five: 0,
    environmental_impact_ten: 0
}

const calc_form = document.getElementById('ws-calc-inputs');
const calc_inputs = calc_form.getElementsByTagName('input');

$(function () {
    $('input[name="cost_water"]').val(calc_values.cost_water);
    $('input[name="cost_electrical"]').val(calc_values.cost_electrical);

    $('.submit-button').on('click', function(e){
        e.preventDefault();
        $('.to-reveal').removeClass('hidden');
    })

    $('#download-button').on('click', function(e){
        e.preventDefault();
        CreateResultsPDF();
    });
});

for (let cinput of calc_inputs) {
    let status = cinput.getAttribute('name');
    cinput.addEventListener('change', updateCalcValue);
}

function numberFormat(number) {
    return new Intl.NumberFormat().format(number);
}

function dollarFormat(number) {
    // return numberFormat(Math.round((number + Number.EPSILON) * 100) / 100);
    return (number).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
}

function updateCalcValue(e) {
    calc_values[e.target.name] = e.target.value;
    Calculate(calc_values);
}

function Calculate(calc_values) {

    //ANNUAL
    calc_values.energy_consumption_savings = calc_values.number_beds * calc_values.pads_per_bed_per_day * calc_values.days_mopped_per_week * calc_values.weeks_mopped_per_year * 0.61 * 1990 * 0.00029307;

    calc_values.energy_cost_savings = calc_values.energy_consumption_savings * calc_values.cost_electrical;

    calc_values.water_consumption_savings = calc_values.number_beds * calc_values.pads_per_bed_per_day * calc_values.days_mopped_per_week * calc_values.weeks_mopped_per_year * 1.355;

    calc_values.water_cost_savings = calc_values.water_consumption_savings * calc_values.cost_water;


    //PROJECTED
    calc_values.economic_impact_three = (calc_values.energy_cost_savings + calc_values.water_cost_savings) * 3;

    calc_values.economic_impact_five = (calc_values.energy_cost_savings + calc_values.water_cost_savings) * 5;

    calc_values.economic_impact_ten = (calc_values.energy_cost_savings + calc_values.water_cost_savings) * 10;

    calc_values.environmental_impact_three = calc_values.water_consumption_savings * 3;

    calc_values.environmental_impact_five = calc_values.water_consumption_savings * 5;

    calc_values.environmental_impact_ten = calc_values.water_consumption_savings * 10;

    displayValues(calc_values);

}

function displayValues(calc_values) {
    
    $('.output').each(function () {
        var output_name = $(this).attr('id');
        var data_type = $(this).attr('data-type');
        var output_value;

        if (data_type == "dollar") {
            output_value = dollarFormat(calc_values[output_name]);
        } else {
            output_value = numberFormat(Math.round(calc_values[output_name]));
        }

        $(this).html(output_value);

    })

}


/*=============================================
=            PDF DOWNLOAD            =
=============================================*/

function CreateResultsPDF() {
    var HTML_Width = $(".results-download-area").width();
    var HTML_Height = $(".results-download-area").height();
    var top_left_margin = 15;
    var PDF_Width = HTML_Width + (top_left_margin * 2);
    var PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
    var canvas_image_width = HTML_Width/1.5;
    var canvas_image_height = HTML_Height/1.5;

    var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;

    html2canvas($(".results-download-area")[0]).then(function (canvas) {
        var imgData = canvas.toDataURL("image/jpeg", 1.0);
        var pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
        pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);
        for (var i = 1; i <= totalPDFPages; i++) { 
            pdf.addPage(PDF_Width, PDF_Height);
            pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
        }
        pdf.save("Contect_WaterSavingsResults.pdf");
        //$(".html-content").hide();
    });


}



/*=====  End of PDF DOWNLOAD  ======*/

